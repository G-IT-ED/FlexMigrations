﻿using System;
using FlexMigrations;
using log4net;

namespace SqlServerMigrationSample
{
  public static class MigrationsFacade
  {
    private static readonly ILog Log = LogManager.GetLogger(nameof(MigrationsFacade));

    public static void Run(string connectionStr)
    {
      try
      {
        MigrationManager.RunMigrations(connectionStr, ErrorPolicy.AllOrNothing, true);
      }
      catch (Exception e)
      {
        Log.Error("Failed to migrate DB: " + e.Message);
      }
    }
  }
}
