﻿using System;
using System.IO;
using log4net;
using log4net.Config;

namespace SqlServerMigrationSample
{
  public static class Program
  {
    private static readonly ILog Log = LogManager.GetLogger(nameof(Program));

    public static void Main()
    {
	  XmlConfigurator.Configure(new FileInfo("log4net.config"));
	  const string connectionStr = "Data Source=localhost;Initial Catalog=FLEX_TEST;User ID=sa;Password=123;";
      MigrationsFacade.Run(connectionStr);

      Console.WriteLine("Finished. Press any key");
      Console.ReadLine();
    }
  }
}
