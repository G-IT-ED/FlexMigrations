﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FlexMigrations;
using Npgsql;

namespace PostgreSqlMigrationSample
{
  public class MigrationFacade
  {
    public  void Run(string connectionString)
    {
      try
      {
        IDbConnectionFactory connectionFactory = new DbConnectionFactory<NpgsqlConnection>(connectionString);
        MigrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, false);
      }
      catch (Exception e)
      {
        Console.WriteLine("Failed to migrate DB: " + e.Message);
      }
    } 


  }

  internal class EhmVersionTableScripts : IVersionTableScripts
  {
    private const string TableName = "settings";
    private const string KeyName = "param_key";
    private const string KeyValue = "DB_VERSION";
    private const string ValueName = "param_value";
    private const string DateName = "date_last_changed";

    public List<string> CreateScripts => null;
    public string CheckExistence => $"SELECT EXISTS(SELECT 1  FROM   information_schema.tables  WHERE  table_name = '{TableName}')";

    public string GetDbVersion => $"select {ValueName} from settings where {KeyName} = '{KeyValue}'";

    public string SetDbVersion(int version, string scriptName) =>
      $"UPDATE {TableName} SET {ValueName} = '{scriptName}', {DateName} = GETDATE() WHERE {KeyName} = '{KeyValue}'";

    public bool ProcessCheckExistenceRes(object sqlRes)
    {
      return (bool)sqlRes;
    }

    public int ProcessGetDbVersionRes(object sqlRes)
    {
      return int.Parse(Regex.Match((string)sqlRes, "(\\d+)_").Groups[1].Value);
    }
  }
}
