﻿using System;
using System.IO;
using log4net.Config;

namespace PostgreSqlMigrationSample
{
  class Program
  {
    static void Main(string[] args)
    {
      XmlConfigurator.Configure(new FileInfo("log4net.config"));
      const string connectionStr = "Host=192.168.142.229;Port=5432;Username=postgres;Password=iskratel;Database=ehm-dlym";
      MigrationFacade facade = new MigrationFacade();
      facade.Run(connectionStr);
      Console.WriteLine("Finished. Press any key");
      Console.ReadLine();
    }
  }
}
