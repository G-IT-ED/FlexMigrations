﻿using System;
using System.Collections.Generic;
using System.Data;

namespace FlexMigrations
{
  public interface IDbConnectionFactory
  {
    DbType DbType { get; }
    IDbConnection CreateConnection();
  }

  public class DbConnectionFactory<T>: IDbConnectionFactory where T : IDbConnection
  {
    private readonly string _connectionString;

    private readonly Dictionary<string, DbType> _dbTypes = new Dictionary<string, DbType>
    {
      {"System.Data.SqlClient.SqlConnection", DbType.SqlServer },
      {"System.Data.SQLite.SQLiteConnection", DbType.SQLite },
      {"Npgsql.NpgsqlConnection", DbType.PostgreSql },
    };

    public DbConnectionFactory(string connectionString)
    {
      _connectionString = connectionString;

      string dbTypeStr = typeof(T).ToString();
      if (!_dbTypes.ContainsKey(dbTypeStr))
        throw new ArgumentException($"Connection type \"{dbTypeStr}\" is not supported");

      DbType = _dbTypes[dbTypeStr];
    }

    public DbType DbType { get; }

    public IDbConnection CreateConnection()
    {
      T res = (T)Activator.CreateInstance(typeof(T));
      res.ConnectionString = _connectionString;
      return res;
    } 
  }
}
