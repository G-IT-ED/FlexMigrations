﻿using System.Collections.Generic;

namespace FlexMigrations
{
  public interface IVersionTableScripts
  {
    /// <summary>
    /// Scripts for creating 'DB version' table in DB.
    /// Should include script for setting initial version value into newly created table.
    /// </summary>
    List<string> CreateScripts { get; }

    /// <summary>
    /// Script for checking existence of 'DB version' table.
    /// Should return 1 column
    /// </summary>
    string CheckExistence { get; }

    /// <summary>
    /// Script for retrieving current DB version.
    /// Should return 1 column which contains version
    /// </summary>
    string GetDbVersion { get; }

    /// <summary>
    /// Script for updating DB version
    /// </summary>
    /// <param name="version">new version</param>
    /// <param name="scriptName">name of upgrade script</param>
    /// <returns></returns>
    string SetDbVersion(int version, string scriptName);

    /// <summary>
    /// Converts result of CheckExistence query to boolean value.
    /// </summary>
    /// <param name="sqlRes">Result of CheckExistence query</param>
    /// <returns>true if the table exists and false otherwise.</returns>
    bool ProcessCheckExistenceRes(object sqlRes);

    /// <summary>
    /// Converts result of GetDbVersion query to int value.
    /// <param name="sqlRes">Result of CheckExistence query</param>
    /// </summary>
    /// <returns>Current DB version - int</returns>
    int ProcessGetDbVersionRes(object sqlRes);
  }
}