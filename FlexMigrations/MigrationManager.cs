﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FlexMigrations.Implementation;
using FlexMigrations.Utils;
using log4net;

namespace FlexMigrations
{
  
  // ReSharper disable once UnusedMember.Global
  public enum ErrorPolicy
  {
    AllOrNothing,
    AsMuchAsPossible
  }

  // ReSharper disable once UnusedMember.Global
  public enum DbType
  {
    SqlServer,
    // ReSharper disable once InconsistentNaming
    SQLite,
    PostgreSql,
  }

  public static class MigrationManager
  {
    static MigrationManager()
    {
      Log = LogUtil.GetLog();
    }

    private static readonly ILog Log;

    /// <summary>
    /// Obsolete overload of RunMigrations method.
    /// Migrates DB to latest version; SQLServer only
    /// </summary>
    /// <param name="connectionString">DB connection string</param>
    /// <param name="errorPolicy">Determines behaviour in case of error during migration. AllOrNothing rollbacks all changes; AsMuchAsPossible rollbacks only failed migration, applying all previous and then stops</param>
    /// <param name="lockDb">Whether to put lock on DB during migration process. SQL Server: true puts DB into RESTRICTED USER mode.</param>
    /// <param name="versionTableScripts">Custom scripts for user-defined version table. In case of null the default implementation is used.</param>
    /// <param name="preFlexDbVersionQuery">Migration from legacy DB Version table to the new one. If set then must contain query which returns current DB version from legacy table.</param>
    /// <param name="assembly">Assembly which contains migration scripts. By default the calling assembly is used.</param>
    /// <param name="cmdTimeoutSec">The time in seconds to wait for an each SQL migration command to execute</param>
    [Obsolete]
    public static bool RunMigrations(
      string connectionString,
      ErrorPolicy errorPolicy,
      bool lockDb,
      IVersionTableScripts versionTableScripts = null,
      string preFlexDbVersionQuery = null,
      Assembly assembly = null,
      int cmdTimeoutSec = 30)
    {
      if (assembly == null)
        assembly = Assembly.GetCallingAssembly();

      return RunMigrations(
          new DbConnectionFactory<SqlConnection>(connectionString),
          errorPolicy,
          lockDb,
          versionTableScripts,
          preFlexDbVersionQuery,
          assembly,
          cmdTimeoutSec);
    }

    /// <summary>
    /// Migrates DB to latest version
    /// </summary>
    /// <param name="connectionFactory">DB connection factory</param>
    /// <param name="errorPolicy">Determines behaviour in case of error during migration. AllOrNothing rollbacks all changes; AsMuchAsPossible rollbacks only failed migration, applying all previous and then stops</param>
    /// <param name="lockDb">Whether to put lock on DB during migration process. SQL Server: true puts DB into RESTRICTED USER mode.</param>
    /// <param name="versionTableScripts">Custom scripts for user-defined version table. In case of null the default implementation is used.</param>
    /// <param name="preFlexDbVersionQuery">Migration from legacy DB Version table to the new one. If set then must contain query which returns current DB version from legacy table.</param>
    /// <param name="assembly">Assembly which contains migration scripts. By default the calling assembly is used.</param>
    /// <param name="cmdTimeoutSec">The time in seconds to wait for an each SQL migration command to execute</param>
    public static bool RunMigrations(
      IDbConnectionFactory connectionFactory,
      ErrorPolicy errorPolicy,
      bool lockDb,
      IVersionTableScripts versionTableScripts = null,
      string preFlexDbVersionQuery = null,
      Assembly assembly = null,
      int cmdTimeoutSec = 30)
    {
      if (connectionFactory == null)
        throw new ArgumentNullException(nameof(connectionFactory));

      if (assembly == null)
        assembly = Assembly.GetCallingAssembly();

      using (Migrator migrator = MigratorFactory.CreateMigrator(connectionFactory, versionTableScripts, cmdTimeoutSec))
      {
        migrator.PrepareVersionTable(preFlexDbVersionQuery);

        int curVersion = migrator.GetCurrentDbVersion();
        List<SqlScript> scripts = PrepareMigrationScripts(curVersion, assembly);

        if (scripts == null)
        {
          Log.WarnFormat("There are no sql scripts in assembly {0}", assembly.FullName);
          return true;
        }

        if (scripts.Count == 0)
        {
          Log.InfoFormat("DB is already at latest version {0}", curVersion);
          return true;
        }

        Log.InfoFormat("Migrating DB from version {0} to {1}...", curVersion, scripts.Last().Index);
        Stopwatch sw = new Stopwatch();
        sw.Start();
        int? resVersion = migrator.Migrate(scripts, errorPolicy, lockDb);
        sw.Stop();

        if (resVersion == null)
        {
          Log.WarnFormat("DB wasn't migrated. The process took {0:F2} seconds", sw.Elapsed.TotalSeconds);
          return false;
        }

        Log.InfoFormat("DB migrated to version {0}. The process took {1:F2} seconds", resVersion, sw.Elapsed.TotalSeconds);
        return true;
      }
    }

    private static List<SqlScript> PrepareMigrationScripts(int curVersion, Assembly assembly)
    {
      List<string> scriptResources = assembly
        .GetManifestResourceNames()
        .Where(s => s.EndsWith(".sql", StringComparison.InvariantCultureIgnoreCase)).ToList();

      if (scriptResources.Count == 0)
        return null;

      return scriptResources
        .Select(x => new SqlScript(x))
        .OrderBy(x => x.Index)
        .Where(x => x.Index > curVersion)
        .Select(x => x.ReadContent(assembly.GetManifestResourceStream(x.FullName)))
        .ToList();
    }
  }
}
