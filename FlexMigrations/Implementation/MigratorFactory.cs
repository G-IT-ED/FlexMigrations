﻿using System;
using System.Collections.Generic;
using FlexMigrations.Implementation.PostgreSql;
using FlexMigrations.Implementation.SqlServer;
using FlexMigrations.Implementation.SQLite;

namespace FlexMigrations.Implementation
{
  internal static class MigratorFactory
  {
    private static readonly Dictionary<DbType, Func<Migrator>> Factory =
      new Dictionary<DbType, Func<Migrator>>
      {
        {DbType.SqlServer, () => new SqlServerMigrator()},
        {DbType.SQLite, () => new SqliteMigrator()},
        {DbType.PostgreSql, () => new PostgreSqlMigrator()},
      };

    public static Migrator CreateMigrator(
      IDbConnectionFactory connectionFactory,
      IVersionTableScripts versionTableScripts,
      int cmdTimeoutSec)
    {
      Migrator res = Factory[connectionFactory.DbType]();
      res.Init(connectionFactory, versionTableScripts, cmdTimeoutSec);
      return res;
    }
  }
}
