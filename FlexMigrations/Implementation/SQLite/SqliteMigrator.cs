﻿using System;
using System.Collections.Generic;

namespace FlexMigrations.Implementation.SQLite
{
  internal class SqliteMigrator: Migrator
  {
    protected override IVersionTableScripts GetVersionTableScripts() => new VersionTableSqliteScripts();

    protected override void ExecuteScript(string script)
    {
      IEnumerable<string> statements = script.Split(';');
      ExecuteNonQueries(statements);
    }

    protected override bool LockDb()
    {
      throw new NotImplementedException("DB locking is not supported for SQLite");
    }

    protected override void UnlockDb()
    {
      throw new NotImplementedException("DB locking is not supported for SQLite");
    }
  }
}
