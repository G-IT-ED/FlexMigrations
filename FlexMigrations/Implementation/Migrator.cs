﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FlexMigrations.Utils;
using log4net;

namespace FlexMigrations.Implementation
{
  internal abstract class Migrator: IDisposable
  {
    static Migrator()
    {
      Log = LogUtil.GetLog();
    }

    public static ILog Log;

    protected abstract bool LockDb();
    protected abstract void UnlockDb();
    protected abstract void ExecuteScript(string script);

    protected abstract IVersionTableScripts GetVersionTableScripts();

    protected IDbConnection _conn;

    private int _cmdTimeout;
    private IDbTransaction _currentTransaction;
    private IDbConnectionFactory _connectionFactory;
    private IVersionTableScripts _versionTableScripts;

    public void Init(IDbConnectionFactory connectionFactory, IVersionTableScripts versionTableScripts, int cmdTimeout)
    {
      _connectionFactory = connectionFactory;
      _cmdTimeout = cmdTimeout;

      if (versionTableScripts == null)
        versionTableScripts = GetVersionTableScripts();

      _versionTableScripts = versionTableScripts;

      CreateConnection();
    }

    public void Dispose()
    {
      _conn.Dispose();
    }

    public void PrepareVersionTable(string preFlexDbVersionQuery)
    {
      try
      {
        object queryRes = ExecuteQuery(_versionTableScripts.CheckExistence);
        bool exists = _versionTableScripts.ProcessCheckExistenceRes(queryRes);
        if (exists)
          return;
      }
      catch
      {
        // Exception here most probably means absence of the table.
        // So, it should be caught and ignored.
      }

      ExecuteInTransaction(() =>
                            {
                              foreach (string script in _versionTableScripts.CreateScripts)
                                ExecuteScript(script);

                              if (!string.IsNullOrEmpty(preFlexDbVersionQuery))
                              {
                                int dbVersion = (int)ExecuteQuery(preFlexDbVersionQuery);
                                ExecuteScript(_versionTableScripts.SetDbVersion(dbVersion, "initial flex"));
                              }
                            });
    }

    public int? Migrate(List<SqlScript> scripts, ErrorPolicy errorPolicy, bool lockDb)
    {
      if (lockDb)
      {
        bool res = LockDb();
        if (!res)
          return null;
      }

      try
      {
        int? resVersion;

        if (errorPolicy == ErrorPolicy.AllOrNothing)
          resVersion = ExecuteAllOrNothing(scripts);
        else
          resVersion = ExecuteAsMuchAsPossible(scripts);

        return resVersion;
      }
      finally
      {
        if (lockDb)
          UnlockDb();
      }
    }

    public int GetCurrentDbVersion()
    {
      object sqlRes = ExecuteQuery(_versionTableScripts.GetDbVersion);
      return _versionTableScripts.ProcessGetDbVersionRes(sqlRes);
    }

    private int? ExecuteAllOrNothing(List<SqlScript> scripts)
    {
      SqlScript currentScript = scripts.First();

      try
      {
        ExecuteInTransaction(() =>
                             {
                               foreach (SqlScript sqlScript in scripts)
                               {
                                 currentScript = sqlScript;

                                 Log.InfoFormat("Applying migration {0}", sqlScript.ShortName);
                                 ExecuteScript(sqlScript.Content);
                               }

                               SetDbVersion(currentScript);
                             });

        return currentScript.Index;
      }
      catch (Exception e)
      {
        Log.Error($"Error during executing script {currentScript.ShortName}", e);
        return null;
      }
    }

    private int? ExecuteAsMuchAsPossible(List<SqlScript> scripts)
    {
      SqlScript previousScript = null;
      foreach (SqlScript sqlScript in scripts)
      {
        try
        {
          ExecuteInTransaction(() =>
                               {
                                 Log.InfoFormat("Applying migration {0}", sqlScript.ShortName);
                                 ExecuteScript(sqlScript.Content);
                                 SetDbVersion(sqlScript);
                               });

          previousScript = sqlScript;
        }
        catch (Exception e)
        {
          Log.Error($"Error during executing script {sqlScript.ShortName}", e);
          return previousScript?.Index;
        }
      }

      return scripts.Last().Index;
    }
    
    private void SetDbVersion(SqlScript latestMigration)
    {
      ExecuteScript(_versionTableScripts.SetDbVersion(latestMigration.Index, latestMigration.ShortName));
    }

    private void CreateConnection()
    {
      _conn?.Dispose();
      _conn = _connectionFactory.CreateConnection();
      _conn.Open();
    }

    protected void CheckConnection()
    {
      if (_conn?.State != ConnectionState.Open)
        CreateConnection();
    }

    private void ExecuteInTransaction(Action action)
    {
      if (_currentTransaction != null)
        throw new ApplicationException("Nested transactions aren't supported");

      using (_currentTransaction = _conn.BeginTransaction())
      {
        try
        {
          action();
          _currentTransaction.Commit();
        }
        catch
        {
          try
          {
            _currentTransaction.Rollback();
          }
          catch
          {
            // intentionally empty
          }

          throw;
        }
        finally
        {
          _currentTransaction = null;
        }
      }
    }

    /// <summary>
    /// Returns value of first column in ResultSet.
    /// Throws if no data.
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    private object ExecuteQuery(string query)
    {
      using (IDbCommand cmd = _conn.CreateCommand())
      {
        cmd.Connection = _conn;
        cmd.CommandText = query;

        using (IDataReader reader = cmd.ExecuteReader())
        {
          reader.Read();
          return reader.GetValue(0);
        }
      }
    }

    protected void ExecuteNonQueries(IEnumerable<string> sqlStatements)
    {
      using (IDbCommand cmd = _conn.CreateCommand())
      {
        cmd.Connection = _conn;
        cmd.Transaction = _currentTransaction;
        cmd.CommandTimeout = _cmdTimeout;

        foreach (string stmt in sqlStatements)
        {
          cmd.CommandText = stmt;
          cmd.ExecuteNonQuery();
        }
      }
    }
  }
}
