﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FlexMigrations.Implementation.SqlServer
{
  internal class SqlServerMigrator : Migrator
  {
    protected override IVersionTableScripts GetVersionTableScripts() => new VersionTableSqlServerScripts();

    protected override void ExecuteScript(string script)
    {
      // All GO should be removed from the script
      
      // 1. Remove single-line comments (optional step)
      script = Regex.Replace(script, @"[\n\r][ \t]*--.*", string.Empty);
      
      // 2. Remove multi-line comments /* */ (go can be inside and erroneously captured by next regex)
      script = Regex.Replace(script, @"\/\*(.|[\n\r])*?\*\/", string.Empty);

      // 3. Split by GO
      // regex is [\n\r]+[ \t]*GO.*
      // [\n\r]+[ \t]*: GO must be preceded with at least one new line, probably followed by spaces or tabs
      // GO.*: GO can optionally be followed by count (GO 2), so a GO is captured with everything till an end of the line
      IEnumerable<string> statements = Regex.Split(script, @"[\n\r]+[ \t]*GO.*", RegexOptions.IgnoreCase).Where(stmt => !string.IsNullOrWhiteSpace(stmt));

      ExecuteNonQueries(statements);
    }

    protected override bool LockDb()
    {
      try
      {
        ExecuteScript($"ALTER DATABASE [{_conn.Database}] SET RESTRICTED_USER WITH ROLLBACK IMMEDIATE");
        return true;
      }
      catch (Exception e)
      {
        Log.Error("Failed to lock DB", e);
        return false;
      }
    }

    protected override void UnlockDb()
    {
      CheckConnection();  // connection can be closed here due to errors in migration scripts. DB should be returned to normal mode in any case
      ExecuteScript($"ALTER DATABASE [{_conn.Database}] SET MULTI_USER");
    }
  }
}
