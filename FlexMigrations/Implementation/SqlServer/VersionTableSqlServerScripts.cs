﻿using System.Collections.Generic;

namespace FlexMigrations.Implementation.SqlServer
{
  internal class VersionTableSqlServerScripts : VersionTableScriptsBase
  {
    public override List<string> CreateScripts =>
      new List<string>
      {
        $"CREATE TABLE {TableName}({Version} int NOT NULL, {ScriptName} nvarchar(50) NOT NULL, {DateName} datetime NOT NULL DEFAULT GETDATE(), " +
        $"CONSTRAINT PK_{TableName} PRIMARY KEY CLUSTERED ({Version} ASC) " +
        "WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) " +
        "ON [PRIMARY]) ON [PRIMARY]",

        $"INSERT INTO {TableName}({Version}, {ScriptName}) VALUES (0, 'initial')"
      };

    public override string CheckExistence => $"SELECT CAST(ISNULL((select object_id('{TableName}', 'u')), 0) AS bit)";
    public override string GetDbVersion => $"SELECT MAX({Version}) FROM {TableName}";
    public override string SetDbVersion(int version, string scriptName) => $"INSERT INTO {TableName}({Version}, {ScriptName}) VALUES ({version}, '{scriptName}')";

    public override bool ProcessCheckExistenceRes(object sqlRes) => (bool) sqlRes;
    public override int ProcessGetDbVersionRes(object sqlRes) => (int) sqlRes;
  }
}
