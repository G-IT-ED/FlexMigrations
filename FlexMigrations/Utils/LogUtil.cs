﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using log4net.Repository;

namespace FlexMigrations.Utils
{
  public class LogUtil
  {
    public static ILog GetLog()
    {
      //Check is repository created
      List<ILoggerRepository> repo = LogManager.GetAllRepositories().ToList();

      //Get Logger
      if (repo.Count==0)
      {
        LogManager.CreateRepository(LogParam.RepositoryName);
        return LogManager.GetLogger(LogParam.RepositoryName, nameof(MigrationManager));
      }
      return LogManager.GetLogger(repo.FirstOrDefault().Name, nameof(MigrationManager));
    }
  }
}