﻿using FlexMigrations.Implementation;

namespace FlexMigrations.Utils
{
  public static class LogParam
  {
    public static string RepositoryName = typeof(Migrator).Name;
  }
}