﻿using System;
using System.IO;
using log4net.Config;

namespace SqliteMigrationSample
{
  class Program
  {
    public static void Main()
    {
      XmlConfigurator.Configure(new FileInfo("log4net.config"));
      const string connectionStr = "Data Source=../../sqlite.db";
      MigrationsFacade.Run(connectionStr);

      Console.WriteLine("Finished. Press any key");
      Console.ReadLine();
    }
  }
}
