﻿using System;
using System.Data.SQLite;
using FlexMigrations;

namespace SqliteMigrationSample
{
  public static class MigrationsFacade
  {
    public static void Run(string connectionStr)
    {
      try
      {
        IDbConnectionFactory connectionFactory = new DbConnectionFactory<SQLiteConnection>(connectionStr);
        MigrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, false);
      }
      catch (Exception e)
      {
        Console.WriteLine("Failed to migrate DB: " + e.Message);
      }
    }
  }
}
